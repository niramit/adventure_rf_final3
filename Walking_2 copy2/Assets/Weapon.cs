﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{

	public float fireRate = 0;
	public float Damage = 10;
	public LayerMask whatToHit;
	public Char2D LeftRightCode;

	public Transform BulletTrailPrefab;

	public Transform MuzzleFlashPrefab;

	float TimetoSpawnEffect = 0;
	public float EffectSpawnRate = 10;

	float timeToFire = 0;
	Transform firePoint;

	void Awake()
	{
		firePoint = transform.FindChild ("BulletLaunchPoint");
		if (firePoint == null) 
		{
			Debug.LogError ("No Firepoint ! What?");
		}
	}

	void Start () 
	{
		//Camshake = GameMaster.GetComponent<CameraShaker> ();
	}

	void Update ()
	{
		if (fireRate == 0) {
			if (Input.GetButtonDown ("Fire1")) {
				Shoot ();
			} else if (Input.GetButton ("Fire1") && Time.time > timeToFire) {
				timeToFire = Time.time + 1 / fireRate;
				Shoot ();
			}
		}
	}

	void Shoot()
	{
		Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition - firePointPosition, 100, whatToHit);
		if (Time.time >= TimetoSpawnEffect) 
		{
			Effect ();
			TimetoSpawnEffect = Time.time + 1 / EffectSpawnRate;

		}
		Debug.DrawLine (firePointPosition,(mousePosition-firePointPosition) * 10, Color.cyan);
		if (hit.collider != null) 
		{
			Debug.DrawLine (firePointPosition, hit.point, Color.red);
			Debug.Log ("We hit " + hit.collider.name + " and did " + Damage + "damage.");
		}

	}

	void Effect()
	{
		if (LeftRightCode.m_FacingRight == true) 
		{
			Debug.Log ("Normal Instantiate");
			Instantiate (BulletTrailPrefab, firePoint.position, firePoint.rotation);
		} else 
		{
			Debug.Log (" ELSEEEEEEEE ");
			Quaternion q = firePoint.rotation;
			float temp = q.z;
			q.z = q.w;
			q.w = temp;

			Instantiate (BulletTrailPrefab, firePoint.position, q);
		}
		print (firePoint.rotation);
		print (firePoint.rotation.x);
		print (firePoint.rotation.y);
		print (firePoint.rotation.z);

		Transform clone = Instantiate (MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
		clone.parent = firePoint;
		float size = Random.Range (0.15f, 0.4f);
		clone.localScale = new Vector3 (size, size, size);
		Destroy (clone.gameObject, 0.05f);
	}
}
