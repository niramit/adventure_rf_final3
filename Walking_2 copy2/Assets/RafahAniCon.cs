﻿using UnityEngine;
using System.Collections;

public class RafahAniCon : MonoBehaviour {
	public Animator useAnimator;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)||Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
			useAnimator.SetBool ("walkAni", true);
		} else {
			useAnimator.SetBool ("walkAni", false);
		}
		if (Input.GetKey (KeyCode.Space) ) {
			useAnimator.SetBool ("jumpAni", true);
		} 
		if (Input.GetKey (KeyCode.RightArrow) && Input.GetKeyDown (KeyCode.Space) || Input.GetKey (KeyCode.D) && Input.GetKeyDown (KeyCode.Space)) {
			useAnimator.SetBool ("jumpAni", true);
		}
		if (Input.GetKey (KeyCode.LeftArrow) && Input.GetKeyDown (KeyCode.Space) || Input.GetKey (KeyCode.A) && Input.GetKeyDown (KeyCode.Space)) {
			useAnimator.SetBool ("jumpAni", true);
		}
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("ground")) {
			this.useAnimator.SetBool ("jumpAni", false);
		}
	}
}
