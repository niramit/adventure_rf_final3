﻿using UnityEngine;
using System.Collections;

public class BatTrigger : MonoBehaviour 
{
	public BatBehavior RafahMaLaew;

	void Start () 
	{
		RafahMaLaew.findRafa = false;
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		RafahMaLaew.findRafa = true;
	}
}
