﻿using UnityEngine;
using System.Collections;

public class Goat : MonoBehaviour, IMonster {
	public Rigidbody2D rb;
	public GameObject key;
	private float s;
	private int GhostHealth;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;
	public GameObject Health5;
	public GameObject Health6;
	public GameObject Health7;
	public GameObject Health8;
	public GameObject Health9;
	public GameObject Health10;
	public GameObject Health11;
	public GameObject Health12;
	public GameObject Health13;
	public GameObject Health14;
	public GameObject Health15;
	public GameObject Health16;
	public GameObject Health17;

	public GameObject HealthBG;


	void Start () {
		rb = GetComponent<Rigidbody2D>();
		s = -4;
		GhostHealth = 17;
	}
	
	// Update is called once per frame
	void Update () {
		/*	this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (s, 0);
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x < -4) 
		{
			this.gameObject.GetComponent<Rigidbody2D> ().velocity += new Vector2 (4, 0);
		}
		if (this.gameObject.GetComponent<Rigidbody2D> ().velocity.x > 4) {
			this.gameObject.GetComponent<Rigidbody2D> ().velocity -= new Vector2 (4, 0);
		}
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
	}
	*/
	}
	public void hit()
	{
		GhostHealth--;
		if (GhostHealth == 16) 
		{
			Destroy (Health1.gameObject, 0.0f);
			//key.transform.Translate (new Vector3 (0, 43, 0));
		}
		if (GhostHealth == 15) 
		{
			Destroy (Health2.gameObject, 0.0f);
		}
		if (GhostHealth == 14) 
		{
			Destroy (Health3.gameObject, 0.0f);
		}
		if (GhostHealth == 13) 
		{
			Destroy (Health4.gameObject, 0.0f);
		}
		if (GhostHealth == 12) 
		{
			Destroy (Health5.gameObject, 0.0f);
		}
		if (GhostHealth == 11) 
		{
			Destroy (Health6.gameObject, 0.0f);
		}
		if (GhostHealth == 10) 
		{
			Destroy (Health7.gameObject, 0.0f);
		}
		if (GhostHealth == 9) 
		{
			Destroy (Health8.gameObject, 0.0f);
		}
		if (GhostHealth == 8) 
		{
			Destroy (Health9.gameObject, 0.0f);
		}
		if (GhostHealth == 7) 
		{
			Destroy (Health10.gameObject, 0.0f);
		}
		if (GhostHealth == 6) 
		{
			Destroy (Health11.gameObject, 0.0f);
		}
		if (GhostHealth == 5) 
		{
			Destroy (Health12.gameObject, 0.0f);
		}
		if (GhostHealth == 4) 
		{
			Destroy (Health13.gameObject, 0.0f);
		}
		if (GhostHealth == 3) 
		{
			Destroy (Health14.gameObject, 0.0f);
		}
		if (GhostHealth == 2) 
		{
			Destroy (Health15.gameObject, 0.0f);
		}
		if (GhostHealth == 1) 
		{
			Destroy (Health16.gameObject, 0.0f);
		}
		if (GhostHealth == 0) 
		{
			Destroy (this.gameObject, 0.0f);
			Destroy (HealthBG.gameObject, 0.0f);
			Destroy (Health17.gameObject, 0.0f);
		}
	}
}
