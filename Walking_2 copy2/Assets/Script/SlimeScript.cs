﻿using UnityEngine;
using System.Collections;

public class SlimeScript : MonoBehaviour,IMonster 
{

	public Rigidbody2D rb;
	public int SlimeHealth = 4;
	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;
	public GameObject BulletPoint;
	public GameObject Bullet;
	public GameObject AIBG;

	public float DestroyBulletSecond;
	//private bool BulletActive;

	public Animator useAnimator;
	private float gunTime = 2.0f;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		//BulletActive = true;
	}

	void Update ()
	{
		/*if (BulletActive == true) 
		{
//			useAnimator.SetBool ("hit", true);
			GameObject munObject = Instantiate (Bullet, BulletPoint.transform.position, Quaternion.identity) as GameObject;
			munObject.name = "Play BallLeft ";
			BulletActive = false;
			StartCoroutine (gunWait ());
		} 
		else
		{
			//useAnimator.SetBool ("hit", false);
		}
	}*/

	}

	public void hit()
	{
		print ("hit");
		SlimeHealth--;
		if (SlimeHealth == 3) 
		{
			Destroy (Health1.gameObject, 0.0f);
		}
		if (SlimeHealth == 2) 
		{
			Destroy (Health2.gameObject, 0.0f);
		}
		if (SlimeHealth == 1) 
		{
			Destroy (Health3.gameObject, 0.0f);
		} 
		if (SlimeHealth == 0) 
		{
			Destroy (Health4.gameObject, 0.0f);
			Destroy (this.gameObject, 0.0f);
			Destroy (AIBG.gameObject, 0.0f);
		}
	}

	IEnumerator gunWait()
	{
		yield return new WaitForSeconds(gunTime);
		//BulletActive = true;
	}
}
