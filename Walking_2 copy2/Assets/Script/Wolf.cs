﻿using UnityEngine;
using System.Collections;

public class Wolf : MonoBehaviour, IMonster
{

	public Rigidbody2D rb;
	private float s;
	public Animator useAnimator;

	public int WolfHealth;

	public GameObject Health1;
	public GameObject Health2;
	public GameObject Health3;
	public GameObject Health4;
	public GameObject Health5;
	public GameObject Health6;
	public GameObject Health7;
	public GameObject Health8;
	public GameObject Health9;
	public GameObject Health10;
	public GameObject Health11;
	public GameObject Health12;
	public GameObject Health13;
	public GameObject Health14;
	public GameObject Health15;
	public GameObject Health16;

	public GameObject HealthBG;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		s = -4;

		WolfHealth = 15;
	}

	void Update () 
	{
		this.transform.Translate (new Vector3 (s, 0, 0) * Time.deltaTime);
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag ("next") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next") )
		{
			transform.rotation = Quaternion.Euler(0,180,0);
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			s *= -1;
		}
		if (other.gameObject.CompareTag ("next2") )
		{
			transform.rotation = Quaternion.Euler(0,0,0);
		}
		if (other.gameObject.CompareTag ("Player")) {
			useAnimator.SetBool ("hit", true);
		}
	}
	void OnCollisionExit2D(Collision2D other)
	{
		useAnimator.SetBool ("hit", false);
	}

	public void hit()
	{
		print ("hit");
		WolfHealth--;
		if (WolfHealth == 15) 
		{
			Destroy (Health1.gameObject, 0.0f);
		}
		if (WolfHealth == 14) 
		{
			Destroy (Health2.gameObject, 0.0f);
		}
		if (WolfHealth == 13) 
		{
			Destroy (Health3.gameObject, 0.0f);
		}
		if (WolfHealth == 12) 
		{
			Destroy (Health4.gameObject, 0.0f);
		}
		if (WolfHealth == 11) 
		{
			Destroy (Health5.gameObject, 0.0f);
		}
		if (WolfHealth == 10) 
		{
			Destroy (Health6.gameObject, 0.0f);
		}
		if (WolfHealth == 9) 
		{
			Destroy (Health7.gameObject, 0.0f);
		}
		if (WolfHealth == 8) 
		{
			Destroy (Health8.gameObject, 0.0f);
		}
		if (WolfHealth == 7) 
		{
			Destroy (Health9.gameObject, 0.0f);
		}
		if (WolfHealth == 6) 
		{
			Destroy (Health10.gameObject, 0.0f);
		}
		if (WolfHealth == 5) 
		{
			Destroy (Health11.gameObject, 0.0f);
		}
		if (WolfHealth == 4) 
		{
			Destroy (Health12.gameObject, 0.0f);
		}
		if (WolfHealth == 3) 
		{
			Destroy (Health13.gameObject, 0.0f);
		}
		if (WolfHealth == 2) 
		{
			Destroy (Health14.gameObject, 0.0f);
		}
		if (WolfHealth == 1) 
		{
			Destroy (Health15.gameObject, 0.0f);
		}
		if (WolfHealth == 0) 
		{
			Destroy (this.gameObject, 0.0f);
			Destroy (HealthBG.gameObject, 0.0f);
			Destroy (Health16.gameObject, 0.0f);
		}
	}
}
