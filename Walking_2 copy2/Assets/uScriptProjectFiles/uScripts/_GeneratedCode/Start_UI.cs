//uScript Generated Code - Build 1.0.2991
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Start_UI : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_1_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_1_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_16_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_17_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_17_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_18_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_21_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_21_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_22_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.AudioClip local_25_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.AudioClip local_28_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_3_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_3_UnityEngine_GameObject_previous = null;
   UnityEngine.AudioClip local_31_UnityEngine_AudioClip = default(UnityEngine.AudioClip);
   UnityEngine.GameObject local_34_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_34_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_35_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_35_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_36_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_36_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_37_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_37_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_40_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_40_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_41_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_41_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_42_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_42_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_43_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_43_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_87_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_87_UnityEngine_GameObject_previous = null;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_2 = "Change";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_2 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_2 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_Quit logic_uScriptAct_Quit_uScriptAct_Quit_5 = new uScriptAct_Quit( );
   bool logic_uScriptAct_Quit_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_7 = "Level2";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_7 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_7 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_9 = "Walking_Mechanic";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_9 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_9 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_11 = "Level3";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_11 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_11 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_13 = "Level4";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_13 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_13 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_13 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_14 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_14 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_14;
   bool logic_uScriptAct_LoadAudioClip_Out_14 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_15 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_15 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_15 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_19 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_19 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_19;
   bool logic_uScriptAct_LoadAudioClip_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_20 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_20 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_20 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_23 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_23;
   bool logic_uScriptAct_LoadAudioClip_Out_23 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_24 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_24 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_26 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_26 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_26;
   bool logic_uScriptAct_LoadAudioClip_Out_26 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_27 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_27 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_27 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_29 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_29 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_29;
   bool logic_uScriptAct_LoadAudioClip_Out_29 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_30 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_30 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_30 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadAudioClip logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_32 = new uScriptAct_LoadAudioClip( );
   System.String logic_uScriptAct_LoadAudioClip_name_32 = "";
   UnityEngine.AudioClip logic_uScriptAct_LoadAudioClip_audioClip_32;
   bool logic_uScriptAct_LoadAudioClip_Out_32 = true;
   //pointer to script instanced logic node
   uScriptAct_PlayAudioSource logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33 = new uScriptAct_PlayAudioSource( );
   UnityEngine.GameObject[] logic_uScriptAct_PlayAudioSource_target_33 = new UnityEngine.GameObject[] {};
   UnityEngine.AudioClip logic_uScriptAct_PlayAudioSource_audioClip_33 = default(UnityEngine.AudioClip);
   bool logic_uScriptAct_PlayAudioSource_Out_33 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_39 = "Start_scene";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_39 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_39 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_39 = true;
   
   //event nodes
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_1_UnityEngine_GameObject = GameObject.Find( "PlayGame" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
      if ( null == local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_3_UnityEngine_GameObject = GameObject.Find( "Exit" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_4;
               }
            }
         }
      }
      if ( null == local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_17_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_21_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_34_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_35_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_36_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_37_UnityEngine_GameObject = GameObject.Find( "Canvas" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_40_UnityEngine_GameObject = GameObject.Find( "Level1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_40_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_40_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_8;
               }
            }
         }
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_40_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_40_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_40_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_8;
               }
            }
         }
      }
      if ( null == local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_41_UnityEngine_GameObject = GameObject.Find( "Level2" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_41_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_6;
               }
            }
         }
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_41_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_41_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_6;
               }
            }
         }
      }
      if ( null == local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_42_UnityEngine_GameObject = GameObject.Find( "Level3" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_42_UnityEngine_GameObject_previous != local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_42_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_10;
               }
            }
         }
         
         local_42_UnityEngine_GameObject_previous = local_42_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_42_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_42_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_10;
               }
            }
         }
      }
      if ( null == local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_43_UnityEngine_GameObject = GameObject.Find( "Level4" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_43_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_43_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_12;
               }
            }
         }
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_43_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_43_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_43_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_12;
               }
            }
         }
      }
      if ( null == local_87_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_87_UnityEngine_GameObject = GameObject.Find( "MainMenu" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_87_UnityEngine_GameObject_previous != local_87_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_87_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_87_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_38;
               }
            }
         }
         
         local_87_UnityEngine_GameObject_previous = local_87_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_87_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_87_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_87_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_38;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_1_UnityEngine_GameObject_previous != local_1_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_1_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_0;
               }
            }
         }
         
         local_1_UnityEngine_GameObject_previous = local_1_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_1_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_1_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_0;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_4;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_40_UnityEngine_GameObject_previous != local_40_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_40_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_40_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_8;
               }
            }
         }
         
         local_40_UnityEngine_GameObject_previous = local_40_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_40_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_40_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_40_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_8;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_41_UnityEngine_GameObject_previous != local_41_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_41_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_6;
               }
            }
         }
         
         local_41_UnityEngine_GameObject_previous = local_41_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_41_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_41_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_6;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_42_UnityEngine_GameObject_previous != local_42_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_42_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_10;
               }
            }
         }
         
         local_42_UnityEngine_GameObject_previous = local_42_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_42_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_42_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_10;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_43_UnityEngine_GameObject_previous != local_43_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_43_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_43_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_12;
               }
            }
         }
         
         local_43_UnityEngine_GameObject_previous = local_43_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_43_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_43_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_43_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_12;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_87_UnityEngine_GameObject_previous != local_87_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_87_UnityEngine_GameObject_previous )
         {
            {
               uScript_Button component = local_87_UnityEngine_GameObject_previous.GetComponent<uScript_Button>();
               if ( null != component )
               {
                  component.OnButtonClick -= Instance_OnButtonClick_38;
               }
            }
         }
         
         local_87_UnityEngine_GameObject_previous = local_87_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_87_UnityEngine_GameObject )
         {
            {
               uScript_Button component = local_87_UnityEngine_GameObject.GetComponent<uScript_Button>();
               if ( null == component )
               {
                  component = local_87_UnityEngine_GameObject.AddComponent<uScript_Button>();
               }
               if ( null != component )
               {
                  component.OnButtonClick += Instance_OnButtonClick_38;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_1_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_1_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_0;
            }
         }
      }
      if ( null != local_3_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_3_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_4;
            }
         }
      }
      if ( null != local_40_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_40_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_8;
            }
         }
      }
      if ( null != local_41_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_41_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_6;
            }
         }
      }
      if ( null != local_42_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_42_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_10;
            }
         }
      }
      if ( null != local_43_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_43_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_12;
            }
         }
      }
      if ( null != local_87_UnityEngine_GameObject )
      {
         {
            uScript_Button component = local_87_UnityEngine_GameObject.GetComponent<uScript_Button>();
            if ( null != component )
            {
               component.OnButtonClick -= Instance_OnButtonClick_38;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.SetParent(g);
      logic_uScriptAct_Quit_uScriptAct_Quit_5.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_14.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_19.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_26.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_29.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.SetParent(g);
      logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_32.SetParent(g);
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded += uScriptAct_LoadLevel_Loaded_2;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded += uScriptAct_LoadLevel_Loaded_7;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Loaded += uScriptAct_LoadLevel_Loaded_9;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11.Loaded += uScriptAct_LoadLevel_Loaded_11;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13.Loaded += uScriptAct_LoadLevel_Loaded_13;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Finished += uScriptAct_PlayAudioSource_Finished_15;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Finished += uScriptAct_PlayAudioSource_Finished_20;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Finished += uScriptAct_PlayAudioSource_Finished_24;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Finished += uScriptAct_PlayAudioSource_Finished_27;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Finished += uScriptAct_PlayAudioSource_Finished_30;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Finished += uScriptAct_PlayAudioSource_Finished_33;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Loaded += uScriptAct_LoadLevel_Loaded_39;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Update( );
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.Loaded -= uScriptAct_LoadLevel_Loaded_2;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded -= uScriptAct_LoadLevel_Loaded_7;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.Loaded -= uScriptAct_LoadLevel_Loaded_9;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11.Loaded -= uScriptAct_LoadLevel_Loaded_11;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13.Loaded -= uScriptAct_LoadLevel_Loaded_13;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Finished -= uScriptAct_PlayAudioSource_Finished_15;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Finished -= uScriptAct_PlayAudioSource_Finished_20;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Finished -= uScriptAct_PlayAudioSource_Finished_24;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Finished -= uScriptAct_PlayAudioSource_Finished_27;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Finished -= uScriptAct_PlayAudioSource_Finished_30;
      logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Finished -= uScriptAct_PlayAudioSource_Finished_33;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.Loaded -= uScriptAct_LoadLevel_Loaded_39;
   }
   
   void Instance_OnButtonClick_0(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_0( );
   }
   
   void Instance_OnButtonClick_4(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_4( );
   }
   
   void Instance_OnButtonClick_6(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_6( );
   }
   
   void Instance_OnButtonClick_8(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_8( );
   }
   
   void Instance_OnButtonClick_10(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_10( );
   }
   
   void Instance_OnButtonClick_12(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_12( );
   }
   
   void Instance_OnButtonClick_38(object o, uScript_Button.ClickEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnButtonClick_38( );
   }
   
   void uScriptAct_LoadLevel_Loaded_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_2( );
   }
   
   void uScriptAct_LoadLevel_Loaded_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_7( );
   }
   
   void uScriptAct_LoadLevel_Loaded_9(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_9( );
   }
   
   void uScriptAct_LoadLevel_Loaded_11(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_11( );
   }
   
   void uScriptAct_LoadLevel_Loaded_13(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_13( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_15(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_15( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_20(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_20( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_24(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_24( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_27(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_27( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_30(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_30( );
   }
   
   void uScriptAct_PlayAudioSource_Finished_33(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_33( );
   }
   
   void uScriptAct_LoadLevel_Loaded_39(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_39( );
   }
   
   void Relay_OnButtonClick_0()
   {
      if (true == CheckDebugBreak("99c93805-3fdd-48c8-89f5-0ffdd1a1730c", "UI_Button_Events", Relay_OnButtonClick_0)) return; 
      Relay_In_14();
   }
   
   void Relay_Loaded_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("98517908-a1a6-4b84-9cb6-5ec3728a0b30", "Load_Level", Relay_Loaded_2)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("98517908-a1a6-4b84-9cb6-5ec3728a0b30", "Load_Level", Relay_In_2)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_2.In(logic_uScriptAct_LoadLevel_name_2, logic_uScriptAct_LoadLevel_destroyOtherObjects_2, logic_uScriptAct_LoadLevel_blockUntilLoaded_2);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_4()
   {
      if (true == CheckDebugBreak("cf27f18e-b428-4b5e-b896-52bc6c61c673", "UI_Button_Events", Relay_OnButtonClick_4)) return; 
      Relay_In_19();
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("538251e4-ee2f-42a3-bfa9-253c647959ff", "Quit", Relay_In_5)) return; 
         {
         }
         logic_uScriptAct_Quit_uScriptAct_Quit_5.In();
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Quit.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_6()
   {
      if (true == CheckDebugBreak("d9c8cc77-b311-4ca9-945a-d9fd69b8444a", "UI_Button_Events", Relay_OnButtonClick_6)) return; 
      Relay_In_26();
   }
   
   void Relay_Loaded_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d9ebe943-5db1-4e21-bfb8-1864bb75e9a3", "Load_Level", Relay_Loaded_7)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d9ebe943-5db1-4e21-bfb8-1864bb75e9a3", "Load_Level", Relay_In_7)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.In(logic_uScriptAct_LoadLevel_name_7, logic_uScriptAct_LoadLevel_destroyOtherObjects_7, logic_uScriptAct_LoadLevel_blockUntilLoaded_7);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_8()
   {
      if (true == CheckDebugBreak("eb44e20d-70a9-4b87-a62d-ae0784a76963", "UI_Button_Events", Relay_OnButtonClick_8)) return; 
      Relay_In_23();
   }
   
   void Relay_Loaded_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("de4009e9-0f0a-4395-b35e-7b0ba9ce4542", "Load_Level", Relay_Loaded_9)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("de4009e9-0f0a-4395-b35e-7b0ba9ce4542", "Load_Level", Relay_In_9)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_9.In(logic_uScriptAct_LoadLevel_name_9, logic_uScriptAct_LoadLevel_destroyOtherObjects_9, logic_uScriptAct_LoadLevel_blockUntilLoaded_9);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_10()
   {
      if (true == CheckDebugBreak("632af52d-86c2-479a-81bf-65fdc783dac8", "UI_Button_Events", Relay_OnButtonClick_10)) return; 
      Relay_In_29();
   }
   
   void Relay_Loaded_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6ed29ab5-ec73-42f7-a441-fd28b6014a7a", "Load_Level", Relay_Loaded_11)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6ed29ab5-ec73-42f7-a441-fd28b6014a7a", "Load_Level", Relay_In_11)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_11.In(logic_uScriptAct_LoadLevel_name_11, logic_uScriptAct_LoadLevel_destroyOtherObjects_11, logic_uScriptAct_LoadLevel_blockUntilLoaded_11);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_12()
   {
      if (true == CheckDebugBreak("e46e59a0-7eb4-4422-8904-9c500c03fee7", "UI_Button_Events", Relay_OnButtonClick_12)) return; 
      Relay_In_32();
   }
   
   void Relay_Loaded_13()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("800ac496-7c53-4da7-9435-174577a620ed", "Load_Level", Relay_Loaded_13)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_13()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("800ac496-7c53-4da7-9435-174577a620ed", "Load_Level", Relay_In_13)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_13.In(logic_uScriptAct_LoadLevel_name_13, logic_uScriptAct_LoadLevel_destroyOtherObjects_13, logic_uScriptAct_LoadLevel_blockUntilLoaded_13);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("294f31da-1fe8-4d20-8060-046db89f1913", "Load_AudioClip", Relay_In_14)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_14.In(logic_uScriptAct_LoadAudioClip_name_14, out logic_uScriptAct_LoadAudioClip_audioClip_14);
         local_16_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_14;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_14.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_15();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Finished_15)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Play_15)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_15 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_15 = local_16_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Play(logic_uScriptAct_PlayAudioSource_target_15, logic_uScriptAct_PlayAudioSource_audioClip_15);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Out;
         
         if ( test_0 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("20c1f4b9-5bb4-479c-badc-7b84d18363a7", "Play_AudioSource", Relay_Stop_15)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_15 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_15 = local_16_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Stop(logic_uScriptAct_PlayAudioSource_target_15, logic_uScriptAct_PlayAudioSource_audioClip_15);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_15.Out;
         
         if ( test_0 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f510257b-f59e-498c-9713-53f2735eccc5", "Load_AudioClip", Relay_In_19)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_19.In(logic_uScriptAct_LoadAudioClip_name_19, out logic_uScriptAct_LoadAudioClip_audioClip_19);
         local_18_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_19;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_19.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_20();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_20()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Finished_20)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_20()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Play_20)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_21_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_20 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_20 = local_18_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Play(logic_uScriptAct_PlayAudioSource_target_20, logic_uScriptAct_PlayAudioSource_audioClip_20);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_20()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cc16b901-2f5a-4e73-9941-bcb9e5de3d17", "Play_AudioSource", Relay_Stop_20)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_21_UnityEngine_GameObject_previous != local_21_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_21_UnityEngine_GameObject_previous = local_21_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_21_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_20 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_20 = local_18_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Stop(logic_uScriptAct_PlayAudioSource_target_20, logic_uScriptAct_PlayAudioSource_audioClip_20);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_20.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_23()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f4c55323-d90d-4bcf-9dc4-2e48ebe4d002", "Load_AudioClip", Relay_In_23)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.In(logic_uScriptAct_LoadAudioClip_name_23, out logic_uScriptAct_LoadAudioClip_audioClip_23);
         local_22_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_23;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_23.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_24();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Finished_24)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Play_24)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_34_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_24 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_24 = local_22_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Play(logic_uScriptAct_PlayAudioSource_target_24, logic_uScriptAct_PlayAudioSource_audioClip_24);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("da1f5315-2209-43b5-ba7d-11ecabf85791", "Play_AudioSource", Relay_Stop_24)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_34_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_24 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_24 = local_22_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Stop(logic_uScriptAct_PlayAudioSource_target_24, logic_uScriptAct_PlayAudioSource_audioClip_24);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_24.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_26()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("30db3ee1-01b5-4b6a-9d1c-c1acac4187a7", "Load_AudioClip", Relay_In_26)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_26.In(logic_uScriptAct_LoadAudioClip_name_26, out logic_uScriptAct_LoadAudioClip_audioClip_26);
         local_25_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_26;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_26.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_27();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Finished_27)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Play_27)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_35_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_27 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_27 = local_25_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Play(logic_uScriptAct_PlayAudioSource_target_27, logic_uScriptAct_PlayAudioSource_audioClip_27);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e9163c16-83a8-4044-8c16-92d6341505fb", "Play_AudioSource", Relay_Stop_27)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_35_UnityEngine_GameObject_previous != local_35_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_35_UnityEngine_GameObject_previous = local_35_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_35_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_27 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_27 = local_25_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Stop(logic_uScriptAct_PlayAudioSource_target_27, logic_uScriptAct_PlayAudioSource_audioClip_27);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_27.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_29()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("2bb3b7c5-4d81-44cc-b87e-0455a25446c4", "Load_AudioClip", Relay_In_29)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_29.In(logic_uScriptAct_LoadAudioClip_name_29, out logic_uScriptAct_LoadAudioClip_audioClip_29);
         local_28_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_29;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_29.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_30();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Finished_30)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Play_30)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_36_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_30 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_30 = local_28_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Play(logic_uScriptAct_PlayAudioSource_target_30, logic_uScriptAct_PlayAudioSource_audioClip_30);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("144b08fa-5718-40a4-a5f3-4dd26e85f30c", "Play_AudioSource", Relay_Stop_30)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_36_UnityEngine_GameObject_previous != local_36_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_36_UnityEngine_GameObject_previous = local_36_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_36_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_30 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_30 = local_28_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Stop(logic_uScriptAct_PlayAudioSource_target_30, logic_uScriptAct_PlayAudioSource_audioClip_30);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_30.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_32()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("661a6c7a-800c-4e04-af00-ce7088ecf2b5", "Load_AudioClip", Relay_In_32)) return; 
         {
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_32.In(logic_uScriptAct_LoadAudioClip_name_32, out logic_uScriptAct_LoadAudioClip_audioClip_32);
         local_31_UnityEngine_AudioClip = logic_uScriptAct_LoadAudioClip_audioClip_32;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_LoadAudioClip_uScriptAct_LoadAudioClip_32.Out;
         
         if ( test_0 == true )
         {
            Relay_Play_33();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load AudioClip.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_33()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Finished_33)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Play_33()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Play_33)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_37_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_33 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_33 = local_31_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Play(logic_uScriptAct_PlayAudioSource_target_33, logic_uScriptAct_PlayAudioSource_audioClip_33);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Out;
         
         if ( test_0 == true )
         {
            Relay_In_13();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Stop_33()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("146da1cc-2f29-403c-a295-30d5dcc689c7", "Play_AudioSource", Relay_Stop_33)) return; 
         {
            {
               List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_37_UnityEngine_GameObject_previous != local_37_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_37_UnityEngine_GameObject_previous = local_37_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               properties.Add((UnityEngine.GameObject)local_37_UnityEngine_GameObject);
               logic_uScriptAct_PlayAudioSource_target_33 = properties.ToArray();
            }
            {
               logic_uScriptAct_PlayAudioSource_audioClip_33 = local_31_UnityEngine_AudioClip;
               
            }
         }
         logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Stop(logic_uScriptAct_PlayAudioSource_target_33, logic_uScriptAct_PlayAudioSource_audioClip_33);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_PlayAudioSource_uScriptAct_PlayAudioSource_33.Out;
         
         if ( test_0 == true )
         {
            Relay_In_13();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Play AudioSource.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnButtonClick_38()
   {
      if (true == CheckDebugBreak("a63c7911-2e30-4c7c-bb60-71baed0e3095", "UI_Button_Events", Relay_OnButtonClick_38)) return; 
      Relay_In_39();
   }
   
   void Relay_Loaded_39()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e416582-1875-466d-91a0-a2f2e7e81974", "Load_Level", Relay_Loaded_39)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_39()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e416582-1875-466d-91a0-a2f2e7e81974", "Load_Level", Relay_In_39)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_39.In(logic_uScriptAct_LoadLevel_name_39, logic_uScriptAct_LoadLevel_destroyOtherObjects_39, logic_uScriptAct_LoadLevel_blockUntilLoaded_39);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Start_UI.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:1", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "1ff697c8-c049-4ba7-8ab3-c5cf833bcf9e", local_1_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:3", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ed4a4692-2c3e-459a-bbe4-a8a56b316d36", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:16", local_16_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5aa0147b-2a06-4c85-b21c-ab8490b356df", local_16_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:17", local_17_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "21bc2018-4a1d-457f-b80c-a222165ca497", local_17_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:18", local_18_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "99def98a-333b-4c9a-b3df-d96ab1ec60c2", local_18_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:21", local_21_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "444b5af3-a497-4384-aeab-7c6ef6b99d31", local_21_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:22", local_22_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ae78b5f8-7182-4eff-ac26-ffe0931e156c", local_22_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:25", local_25_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "82469434-485a-4efb-a741-f0074ab187e1", local_25_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:28", local_28_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "22c04771-43a6-4ead-b7ac-95de8a49f4b1", local_28_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:31", local_31_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "74bb6279-20a5-4978-834a-787018dbdce9", local_31_UnityEngine_AudioClip);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:34", local_34_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e8b76a9d-36d2-4d49-a4d8-643db3aa329e", local_34_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:35", local_35_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a7ae5d5c-7794-4ba0-b12f-b906b0016933", local_35_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:36", local_36_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "12e56009-d445-40f6-8e49-c0bff4f12bfd", local_36_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:37", local_37_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "dd78b7b8-d15f-4b7c-a6b5-2eb2afbaeeff", local_37_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:40", local_40_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "fd1cd565-6a52-46df-a146-1a10e63d7961", local_40_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:41", local_41_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6e15ba6d-aa78-4955-9069-8efb43256150", local_41_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:42", local_42_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "485c1072-f61b-4736-b41f-bb14a957353f", local_42_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:43", local_43_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "45677d0a-ed59-4b8e-9bee-5e112f9f49b2", local_43_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Start_UI.uscript:87", local_87_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c9eeeed7-b8dc-44be-8b03-c2bf3282c5d9", local_87_UnityEngine_GameObject);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
