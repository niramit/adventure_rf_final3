﻿using UnityEngine;
using System.Collections;

public class BulletHell : MonoBehaviour 
{

	public Transform BulletTrailPrefab;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (bossFire());
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator bossFire()
	{
		while (true) {
			yield return new WaitForSeconds(3f);
			startFire ();
		}

	}

	void startFire()
	{
		for(int i = 0; i < 30 ; i++)
		{
			Quaternion q =  this.transform.rotation * Quaternion.Euler(new Vector3(0,0,-(i*15)));
			Instantiate (BulletTrailPrefab, this.transform.position,q);
		}
	}
}
